-----------------------------------------------------------------------------
-- |
-- Modulo       :   Main.hs
--
-- Autores      :   Francisco Martínez (09-10502),
--                  Paul Baptista (10-10056)
--
-- Licencia     :   Apache License 2.0
--
-- Implementation de mosaico-bin, como proyecto académico para ci3661.
-- 
-- Universidad Simón Bolívar
-- Caracas - Venezuela
--
-- Trimestre Abril-Julio 2015
--
-----------------------------------------------------------------------------

module Main (main,ciclo) where

import Data.Maybe

import System.Exit (exitSuccess)

import System.Environment

import Graphics.Mosaico.Diagrama
  ( Diagrama((:-:), (:|:), Hoja)
  , Paso(Primero, Segundo)
  )

import Graphics.Mosaico.Imagen
  ( Imagen(Imagen, altura, anchura, datos)
  , leerImagen
  )

import Graphics.Mosaico.Ventana
  ( Ventana
  , cerrar
  , crearVentana
  , leerTecla
  , mostrar
  )

import Diagramas
  ( Orientación(Horizontal, Vertical)
  , caminar
  , dividir
  , dividiag
  , rectánguloImagen
  , sustituir
  )

import Imagen
  ( colorPromedio
  , hSplit
  , vSplit
  )

-- | Función de 'IO' que muestra en ventana las teclas presionadas por el
-- usuario, para dividir navegar por por la imagen.
--
-- Controles: 
--
--  [@q@]             Salir
--
--  [@h@]             Divide horizontalmente cada una de las ṕiezas del
--                    mosaico
--
--  [@h@]             Divide verticalmente cada una de las ṕiezas del
--                    mosaico
--
--  [@BackSpace@]     Retroceder el enfoque al conjunto superior
--
--  [@←, ↑, →, ↓@]    Si existe, moverse hacia el subdiagrama de la dirección
--                    correspondiente. Si no, dividir el diagrama en la
--                    dirección señalada
--
-- Cualquier otra entrada no tendra efecto, se esperará por una entrada
-- correcta.
--
-- Recibe como parámetros una 'Ventana', un 'Diagrama', y una lista de
-- 'Paso's. La primera para mostrar el mosaico que se recibe como 'Diagrama'
-- enfocando la posición que representa la lista de pasos.
--
ciclo ::
  Ventana       -- ^ 'Ventana' interactiva en la que se mostrará el mosaico.
  -> Diagrama   -- ^ Diagrama' que representa el mosaico que se trabaja.
  -> [Paso]     -- ^ Lista de 'Paso's que representa la posición enfocada.
  -> IO ()
ciclo ventana diagrama pasos =
  do
    mostrar ventana pasos diagrama
    tecla <- leerTecla ventana
    maybe (exitSuccess) (evento) tecla

  where
    evento tecla =
      do
        case tecla of
          "q"         ->  do
                            cerrar ventana
                            exitSuccess

          "Up"        ->  let (diag',pasos') = accion diagrama pasos tecla
                          in ciclo ventana diag' pasos'

          "Down"      ->  let(diag',pasos') = accion diagrama pasos tecla
                          in ciclo ventana diag' pasos'

          "Right"     ->  let (diag',pasos') = accion diagrama pasos tecla
                          in ciclo ventana diag' pasos'

          "Left"      ->  let (diag',pasos') = accion diagrama pasos tecla
                          in ciclo ventana diag' pasos'

          "v"         ->  let (diag',pasos') = accion diagrama pasos tecla
                          in ciclo ventana diag' pasos'

          "h"         ->  let (diag',pasos') = accion diagrama pasos tecla
                          in ciclo ventana diag' pasos'

          "BackSpace" ->  let (diag',pasos') = accion diagrama pasos tecla
                          in ciclo ventana diag' pasos'

          _           ->  ciclo ventana diagrama pasos

        where 
          accion diag ps "Left" = 
            maybe (error "Camino fallido.") accion' (caminar ps diag)
            where
              accion' (_ :|: _) = (diag, ps++[Primero])

              accion' (Hoja rectángulo) =
                let maydiv = (dividir Vertical rectángulo)
                in 
                  if isNothing maydiv then
                    (diag, ps)
                  else
                    (sustituir (fromJust maydiv) ps diag, ps++[Primero])

              accion' _ = (diag, ps)


          accion diag ps "Right" =
            maybe (error "Camino fallido.") accion' (caminar ps diag)
            where
              accion' (_ :|: _) = (diag, ps++[Segundo])

              accion' (Hoja rectángulo) = 
                let maydiv = dividir Vertical rectángulo
                in
                  if isNothing maydiv then
                    (diag, ps)
                  else
                    (sustituir (fromJust maydiv) ps diag, ps++[Segundo])

              accion' _ = (diag, ps)


          accion diag ps "Up" =
            maybe (error "Camino fallido.") accion' (caminar ps diag)
            where
              accion' (_ :-: _) = (diag, ps++[Primero])

              accion' (Hoja rectángulo) =
                let maydiv = dividir Horizontal rectángulo
                in
                  if isNothing maydiv then
                    (diag, ps)
                  else
                    (sustituir (fromJust maydiv) ps diag, ps++[Primero])

              accion' _ = (diag, ps)


          accion diag ps "Down" =
            maybe (error "Camino fallido.") accion' (caminar ps diag)
            where
              accion' (_ :-: _) = (diag, ps++[Segundo])

              accion' (Hoja rectángulo) = 
                let maydiv = dividir Horizontal rectángulo
                in
                  if isNothing maydiv then
                    (diag, ps)
                  else
                    (sustituir (fromJust maydiv) ps diag, ps++[Segundo])

              accion' _ = (diag, ps)

          accion diag ps "v" =
            ( sustituir (dividiag Vertical (fromJust $ caminar ps diag)) ps diag
              , ps )

          accion diag ps "h" =
            ( sustituir (dividiag Horizontal (fromJust $ caminar ps diag)) ps diag
              , ps )

          accion diag [] "BackSpace" = (diag, [])
          accion diag ls "BackSpace" = (diag, init ls)
          accion diag ps _ = (diag, ps)


-- | Función de 'IO()' que lee de el nombre la imagen pasado como argumento
-- y procede a cargarla y ejecutar el ciclo que que permite al usuario 
-- interactuar con la misma y aplicar el efecto de mosaico. 
main :: IO ()
main =
  do 
    args <- getArgs

    if (length args) /= 1 then
      error "Use con exactamente un argumento"
    else do

      eiImg <- leerImagen $ head args

      let
        imagen = either (error) (\a -> a) eiImg
        (x,y)  = (\(Imagen w h _) -> (w,h)) imagen
        fsDiag = Hoja $ rectánguloImagen imagen

      ventana <- crearVentana x y
      ciclo ventana fsDiag []