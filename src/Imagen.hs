-----------------------------------------------------------------------------
-- |
-- Modulo       :   Imagen.hs
--
-- Autores      :   Francisco Martínez (09-10502),
--                  Paul Baptista (10-10056)
--
-- Licencia     :   Apache License 2.0
--
-- Modulo de Imagen, otorga las funcionalidades necesarias para la
-- manipulacion de imagenes que permite la generacion de un efecto
-- de mosaico. Incluye cortes verticales,horizontales, y una funcion
-- para promediar el color de una imagen. 
-- 
-- Universidad Simón Bolívar
-- Caracas - Venezuela
--
-- Trimestre Abril-Julio 2015
--
-----------------------------------------------------------------------------

module Imagen
  ( hSplit, vSplit
  , colorPromedio
  ) where

import Graphics.Mosaico.Imagen 
  ( Color(Color, rojo, verde, azul)
  , Imagen(Imagen, altura, anchura, datos)
  )


-- | Función que genera una imagen como resultado de recortar una 'Imagen'
-- inicial a partir de las dimensiones suministradas.
subImagen ::
  Integer     --  ^ Altura de corte en el eje X
  -> Integer  --  ^ Altura de corte en el eje Y      
  -> Integer  --  ^ Anchura final de la 'Imagen'
  -> Integer  --  ^ Altura final de la 'Imagen'
  -> Imagen   --  ^ Imagen a recortar.
  -> Imagen
subImagen ix iy anc' alt' (Imagen anc alt dat)
  | ix < 0 || iy < 0
    || (ix+anc') > anc 
    || (iy+alt') > alt = error "Fuera de los límites"
  | otherwise = Imagen anc' alt' (cpixel dat)
      where
        cpixel xs =
          map (\x ->  take (fromIntegral anc')
                      (drop (fromIntegral ix) x))
              (take (fromIntegral alt') (drop (fromIntegral iy) xs))

  
-- | Funcion que parte en dos, verticalmente, una 'Imagen' dada. 
vSplit ::
  Imagen              -- ^ 'Imagen' inicial
  -> (Imagen, Imagen) -- ^ Dupla de las imágenes resultantes
vSplit (Imagen anc alt dat ) =
  ( subImagen 0 0 (div anc 2) alt (Imagen anc alt dat ) ,
    subImagen (div anc 2) 0 ((div anc 2)+(mod anc 2)) alt (Imagen anc alt dat)) 


-- | Funcion que parte en dos, verticalmente, una 'Imagen' dada. 
hSplit ::
  Imagen              -- ^ 'Imagen' inicial
  -> (Imagen, Imagen) -- ^ Dupla de las imágenes resultantes
hSplit(Imagen anc alt dat) =
  ( subImagen 0 0 anc (div alt 2) (Imagen anc alt dat ) ,
    subImagen 0 (div alt 2) anc ((div alt 2)+(mod alt 2)) (Imagen anc alt dat))  


-- | Esta funcion devuelve el 'Color' promedio de la imagen dada, siendo este
-- el promedio de 'Rojo', 'Verde' y 'Azul' para todos los pixeles de la imagen.
colorPromedio ::
  Imagen
  -> Color
colorPromedio (Imagen anc alt dat) =
  let
    to' x = toInteger $ fromEnum x
    from' x = toEnum $ fromIntegral x
  in
    (\(x,y,z) -> Color  (from' $ div x (alt * anc))
                        (from' $ div y (alt * anc))
                        (from' $ div z (alt * anc))
    ) $
      foldl ( \(x,y,z) (Color r v a ) ->  ( x + (to' r)
                                          , y + (to' v)
                                          , z + (to' a)
                                          )
            )
            (0,0,0) (concat dat)





































