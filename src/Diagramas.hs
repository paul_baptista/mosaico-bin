-----------------------------------------------------------------------------
-- |
-- Modulo       :   Diagramas.hs
--
-- Autores      :   Francisco Martínez (09-10502),
--                  Paul Baptista (10-10056)
--
-- Licencia     :   Apache License 2.0
--
-- Módulo para manejo de 'Diagrama's. Otorga las funcionalidades necesarias
-- para dividir, recorrer y sustituir 'Diagramas's 
--
-- Universidad Simón Bolívar
-- Caracas - Venezuela
--
-- Trimestre Abril-Julio 2015
--
-----------------------------------------------------------------------------

module Diagramas
  ( rectánguloImagen
  , Orientación(Horizontal, Vertical)
  , dividir
  , dividiag
  , caminar
  , sustituir
  ) where

import Graphics.Mosaico.Diagrama
  ( Rectángulo(Rectángulo, color, imagen)
  , Diagrama((:-:), (:|:), Hoja)
  , Paso(Primero, Segundo)
  )

import Graphics.Mosaico.Imagen
  ( Imagen(Imagen, altura, anchura, datos)
  )

import Imagen
  ( colorPromedio
  , hSplit
  , vSplit
  )

import Data.Maybe

-- | Tipo de datos @Orientación@, usado para describir si la orientacion de 
-- una division será vertical u horizontal.
data Orientación =
    Horizontal
  | Vertical
  deriving Show


-- | Función que a partir de una 'Imagen' genera un rectángulo pintado con el
-- color promedio de la misma
rectánguloImagen :: Imagen -> Rectángulo

rectánguloImagen img = Rectángulo (colorPromedio img) img


-- | Función que realiza una division de un rectangulo, el cual
-- posee visualmente el color promedio de la imagen asociada. Esta
-- division retorna un @Maybe Diagrama@ que, en caso de haber sido exitosa
-- la división, almacena los dos @Rectangulo@s resultantes, que a su vez
-- poseen el color promedio de su subimagen asociada. 
dividir ::
  Orientación       -- ^ @Orientación@ en la que se hará la división
  -> Rectángulo     -- ^ @Rectángulo@ a dividor
  -> Maybe Diagrama 

dividir Horizontal (Rectángulo _ (Imagen an al dat))
  | al < 2 = Nothing
  | otherwise =
    let
      division = hSplit (Imagen an al dat)
    in
      Just $
        (Hoja $ rectánguloImagen $ fst division)
        :-:
        (Hoja $ rectánguloImagen $ snd division)

dividir Vertical (Rectángulo _ (Imagen an al dat))
  | an < 2 = Nothing
  | otherwise =
    let
      division = vSplit (Imagen an al dat)
    in
      Just $
        (Hoja $ rectánguloImagen $ fst division)
        :|:
        (Hoja $ rectánguloImagen $ snd division)


-- | Función que recibe una @Orientación@ y un 'Diagrama' y retorna el 
-- 'Diagrama' que se forma al dividir cada una de las hojas del oroginal en la
-- @Orientación@ indicada
dividiag ::
  Orientación   -- ^ @Orientación@ en la que se hará la división de las hojas
  ->  Diagrama  -- ^ 'Diagrama' original
  -> Diagrama
dividiag orient (Hoja rectángulo) =
  maybe (Hoja rectángulo) (id) (dividir orient rectángulo)
dividiag orient diag = 
  let
    d1 = dividiag orient $ fromJust $ caminar [Primero] diag
    d2 = dividiag orient $ fromJust $ caminar [Segundo] diag
  in
    sustituir d1 [Primero] (sustituir d2 [Segundo] diag)

--fractaldiag (Hoja rectángulo) =
--  maybe (Hoja rectángulo) (div1) (dividir Vertical rectángulo)
--  where
--    div1 diag =
--      let Hoja rectángulo = fromJust $ caminar [Segundo] diag 
--      in sustituir
--        ( maybe (Hoja rectángulo) (div2) (dividir Horizontal rectángulo) ) [Segundo] diag
--    div2 diag = 
--      let Hoja rectángulo = fromJust $ caminar [Primero] diag 
--      in sustituir
--        ( maybe (Hoja rectángulo) (div3) (dividir Vertical rectángulo) ) [Primero] diag
--    div3 diag = 
--      let Hoja rectángulo = fromJust $ caminar [Segundo] diag 
--      in sustituir
--        ( maybe (Hoja rectángulo) (div4) (dividir Horizontal rectángulo) ) [Segundo] diag
--    div4 diag = 
--      let Hoja rectángulo = fromJust $ caminar [Primero] diag 
--      in sustituir
--        ( maybe (Hoja rectángulo) (div4) (dividir Vertical rectángulo) ) [Primero] diag

--fractaldiag diag = 
--  let
--    d1 = fractaldiag (fromJust $ caminar [Primero] diag)
--    d2 = fractaldiag (fromJust $ caminar [Segundo] diag)
--  in
--    sustituir d1 [Primero] (sustituir d2 [Segundo] diag)


-- | Funcion que, dada un 'Diagrama' y una lista de 'Paso's, recorre el
-- 'Diagrama' recibido y devuelve un @Maybe Diagrama@ con el sub'Diagrama'
-- encontrado al finalizar el recorrido. En caso de el recorrido no sea
-- posible, devuelve 'Nothing'.
caminar ::
  [Paso]            -- ^ Pasos recorrer
  -> Diagrama       -- ^ Diagrama a recorrer
  -> Maybe Diagrama 

caminar [] diag = Just diag
caminar _ (Hoja _) = Nothing
caminar (Primero:ls) (diag1 :-: _) = caminar ls diag1
caminar (Primero:ls) (diag1 :|: _) = caminar ls diag1
caminar (Segundo:ls) (_ :-: diag2) = caminar ls diag2
caminar (Segundo:ls) (_ :|: diag2) = caminar ls diag2


-- | Funcion que sustituye un 'Diagrama' A dado, dentro de 
-- un 'Diagrama' B, la ubicacion de la sustitucion en el 
-- subarbol sera dada por la lista de 'Paso's.
sustituir ::
  Diagrama    -- ^ 'Diagrama' a sustituir
  -> [Paso]   -- ^ @Posición@ en la que se hará la sustitución
  -> Diagrama -- ^ 'Diagrama' en el que se hará la sustitución
  -> Diagrama

sustituir d1 [] _ = d1
sustituir _ _ (Hoja r) = Hoja r
sustituir d1 (Primero:ls) (d2 :-: d3) = (sustituir d1 ls d2) :-: d3
sustituir d1 (Primero:ls) (d2 :|: d3) = (sustituir d1 ls d2) :|: d3
sustituir d1 (Segundo:ls) (d2 :-: d3) = d2 :-: (sustituir d1 ls d3)
sustituir d1 (Segundo:ls) (d2 :|: d3) = d2 :|: (sustituir d1 ls d3)